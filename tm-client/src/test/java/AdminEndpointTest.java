import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.anenkov.tm.endpoint.*;
import ru.anenkov.tm.marker.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Category(AllCategory.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class AdminEndpointTest {

    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    private final AdminEndpointService adminEndpointService = new AdminEndpointService();
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @Test
    @Category(CreateCategory.class)
    public void aCreateAndClearBinFile() throws IOException {
        final File file = new File("C:\\Users\\User\\IdeaProjects\\task-manager-java-23\\data.bin");
        Files.deleteIfExists(file.toPath());
        Assert.assertFalse(file.exists());
        final Session session = sessionEndpoint.openSession("admin", "admin");
        adminEndpoint.saveDataBinary(session);
        Assert.assertTrue(file.length() != 0);
        Assert.assertTrue(file.exists());
        adminEndpoint.clearDataBinary(session);
        Assert.assertFalse(file.exists());
    }

    @Test
    @Category(CreateCategory.class)
    public void bCreateAndClearBase64File() throws IOException {
        final File file = new File("C:\\Users\\User\\IdeaProjects\\task-manager-java-23\\database64.base64");
        Files.deleteIfExists(file.toPath());
        Assert.assertFalse(file.exists());
        final Session session = sessionEndpoint.openSession("admin", "admin");
        adminEndpoint.saveDataBase64(session);
        Assert.assertTrue(file.exists());
        Assert.assertTrue(file.length() != 0);
        adminEndpoint.clearDataBase64(session);
        Assert.assertFalse(file.exists());
    }

    @Test
    @Category(UpdateCategory.class)
    public void fLoadBaseFileTest() throws IOException {
        TaskEndpointService taskEndpointService = new TaskEndpointService();
        TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();
        final Session session = sessionEndpoint.openSession("admin", "admin");
        final File file = new File("C:\\Users\\User\\IdeaProjects\\task-manager-java-23\\database64.base64");
        Files.deleteIfExists(file.toPath());
        taskEndpoint.clear(session);
        taskEndpoint.create(session, "name", "description");
        adminEndpoint.saveDataBase64(session);
        Assert.assertFalse(taskEndpoint.findAll(session).isEmpty());
        taskEndpoint.clear(session);
        Assert.assertTrue(taskEndpoint.findAll(session).isEmpty());
        adminEndpoint.loadDataBase64(session);
        Assert.assertTrue(file.exists());
        taskEndpoint.clear(session);
        adminEndpoint.clearDataBase64(session);
    }

    @Test
    @Category(CreateCategory.class)
    public void createAndClearXmlFile() throws IOException {
        final File file = new File("C:\\Users\\User\\IdeaProjects\\task-manager-java-23\\data.xml");
        Files.deleteIfExists(file.toPath());
        Assert.assertFalse(file.exists());
        final Session session = sessionEndpoint.openSession("admin", "admin");
        adminEndpoint.saveDataXml(session);
        Assert.assertTrue(file.length() != 0);
        Assert.assertTrue(file.exists());
        adminEndpoint.clearDataXml(session);
        Assert.assertFalse(file.exists());
    }

    @Test
    @Category(UpdateCategory.class)
    public void eLoadBinFileTest() throws IOException {
        TaskEndpointService taskEndpointService = new TaskEndpointService();
        TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();
        final Session session = sessionEndpoint.openSession("admin", "admin");
        final File file = new File("C:\\Users\\User\\IdeaProjects\\task-manager-java-23\\data.bin");
        Files.deleteIfExists(file.toPath());
        taskEndpoint.clear(session);
        taskEndpoint.create(session, "name", "description");
        adminEndpoint.saveDataBinary(session);
        Assert.assertFalse(taskEndpoint.findAll(session).isEmpty());
        taskEndpoint.clear(session);
        Assert.assertTrue(taskEndpoint.findAll(session).isEmpty());
        adminEndpoint.loadDataBinary(session);
        Assert.assertTrue(file.exists());
        taskEndpoint.clear(session);
        adminEndpoint.clearDataBinary(session);
    }

    @Test
    @Category(CreateCategory.class)
    public void dCreateAndClearJsonFile() throws IOException {
        final File file = new File("C:\\Users\\User\\IdeaProjects\\task-manager-java-23\\data.json");
        Files.deleteIfExists(file.toPath());
        Assert.assertFalse(file.exists());
        final Session session = sessionEndpoint.openSession("admin", "admin");
        adminEndpoint.saveDataJson(session);
        Assert.assertTrue(file.length() != 0);
        Assert.assertTrue(file.exists());
        adminEndpoint.cleanDataJson(session);
        Assert.assertFalse(file.exists());
    }

}
