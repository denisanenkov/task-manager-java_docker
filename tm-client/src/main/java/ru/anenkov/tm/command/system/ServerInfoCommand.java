package ru.anenkov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;

import java.util.Collection;

public class ServerInfoCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "server-info";
    }

    @Override
    public @Nullable String description() {
        return "Info about connecting";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SERVER INFO]");
        System.out.println("[OK]");
    }

}
