package ru.anenkov.tm.command.system;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.endpoint.AdminEndpoint;

public class GetHostPortInfo {

    public @Nullable
    String arg() {
        return "-h";
    }

    public @Nullable String name() {
        return "Server-info";
    }

    public @Nullable String description() {
        return "View post, port";
    }

    public void execute() throws Exception {
        System.out.println("[SERVER INFO]");
        System.out.println("ACTIVE PORT: ");
        System.out.println("[SUCCESS]");
    }

}
