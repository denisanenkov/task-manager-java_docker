package ru.anenkov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

public class TaskCreateClientCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "Task-create";
    }

    @Override
    public @Nullable String description() {
        return "Create task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.print("ENTER TASK NAME: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER TASK DESCRIPTION: ");
        @NotNull final String description = TerminalUtil.nextLine();
        bootstrap.getTaskEndpoint().create(bootstrap.getSession(), name, description);
        System.out.println("[CREATE SUCCESS]");
    }

    @Nullable
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
