#!/bin/bash
if [ ! -f tm-server.pid ]; then
 echo "PID SERVER NOT FOUND"
 exit 1;
fi
echo 'PROCESS WITH PID '$(cat tm-server.pid)' KILLED SUCCESSFULLY';
kill -9 $(cat tm-server.pid)
rm tm-server.pid