#!/bin/bash
echo "RESTART SERVER"
if [ ! -f tm-server.pid ]; then
 echo "SERVER WITH PID NOT FOUND"
 exit 1;
fi
echo 'PROCESS '$(cat tm-server.pid)' WAS KILLED SUCCESSFULLY';
kill -9 $(cat tm-server.pid)
rm tm-server.pid
mkdir -p ../bash/logs
rm -f ../bash/logs/tm-server.log
nohup java -jar ../../tm-server.jar > ../bash/logs/tm-server.log 2>&1 &
echo $! > tm-server.pid
echo "SERVER IS RUNNING WITH PID "$!