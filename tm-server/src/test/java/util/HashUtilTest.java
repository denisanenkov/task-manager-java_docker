package util;

import org.junit.Assert;
import org.junit.Test;
import ru.anenkov.tm.exception.empty.EmptyPasswordException;
import ru.anenkov.tm.util.HashUtil;

public class HashUtilTest {

    @Test
    public void saltTest() {
        String password = "pass";
        Assert.assertEquals(password, "pass");
        String salt = HashUtil.salt(password);
        Assert.assertNotNull(salt);
        Assert.assertNotEquals(password, salt);
    }

    @Test(expected = EmptyPasswordException.class)
    public void saltNegativeTest() {
        String password = null;
        String salt = HashUtil.salt(password);
        Assert.assertNotNull(salt);
        Assert.assertNotEquals(password, salt);
    }

    @Test
    public void md5Test() {
        String password = "pass";
        Assert.assertEquals(password, "pass");
        String salt = HashUtil.salt(password);
        Assert.assertNotNull(salt);
        Assert.assertNotEquals(password, salt);
        String md5 = HashUtil.MD5(salt);
        Assert.assertNotNull(salt);
        Assert.assertNotEquals(md5, salt);
        Assert.assertNotEquals(md5, password);
    }

}
