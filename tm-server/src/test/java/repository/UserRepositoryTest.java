package repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.repository.UserRepository;
import ru.anenkov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public class UserRepositoryTest {

    final UserRepository userRepository = new UserRepository();
    public User user = new User();

    @Before
    public void beforeTest() {
        user.setRole(Role.USER);
        user.setFirstName("Denis");
        user.setLastName("Anenkov");
        user.setMiddleName("Aleksandrovich");
        user.setEmail("denk.an@inbox.ru");
        user.setLogin("test");
        user.setPasswordHash("passTest");
    }

    @Test
    public void testCreate() {
        Assert.assertFalse(user.getId().isEmpty());
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(user);
        Assert.assertFalse(userRepository.findAll().isEmpty());
    }

    @Test
    public void testFind() {
        userRepository.add(user);
        final User user1 = userRepository.findById(user.getId());
        Assert.assertNotNull(user1);
        final User user2 = userRepository.findByLogin(user.getLogin());
        Assert.assertNotNull(user2);
        final User user3 = userRepository.findByEmail(user.getEmail());
        Assert.assertNotNull(user3);
    }

    @Test
    public void testRemove() {
        userRepository.add(user);
        final User user1 = userRepository.removeById(user.getId());
        Assert.assertNotNull(user1);
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(user);
        final User user2 = userRepository.removeByLogin(user.getLogin());
        Assert.assertNotNull(user2);
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(user);
        final User user3 = userRepository.removeByEmail(user.getEmail());
        Assert.assertNotNull(user3);
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void testListUsers() {
        final User user1 = new User("login1", "pass1");
        final User user2 = new User("login2", "pass2");
        final User user3 = new User("login3", "pass3");
        userRepository.add(user1);
        Assert.assertEquals(1, userRepository.getList().size());
        userRepository.add(user2);
        Assert.assertEquals(2, userRepository.getList().size());
        userRepository.add(user3);
        Assert.assertEquals(3, userRepository.getList().size());
    }

}
