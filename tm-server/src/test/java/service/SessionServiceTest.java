package service;

import org.junit.Assert;
import org.junit.Test;
import ru.anenkov.tm.api.repository.ISessionRepository;
import ru.anenkov.tm.api.repository.IUserRepository;
import ru.anenkov.tm.api.service.IPropertyService;
import ru.anenkov.tm.api.service.ISessionService;
import ru.anenkov.tm.api.service.IUserService;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.exception.empty.EmptySignatureSessionException;
import ru.anenkov.tm.exception.empty.EmptyTimestampSessionException;
import ru.anenkov.tm.exception.empty.EmptyUserIdSessionException;
import ru.anenkov.tm.exception.user.AccessDeniedException;
import ru.anenkov.tm.repository.SessionRepository;
import ru.anenkov.tm.repository.UserRepository;
import ru.anenkov.tm.service.PropertyService;
import ru.anenkov.tm.service.SessionService;
import ru.anenkov.tm.service.UserService;

public class SessionServiceTest {

    ISessionRepository sessionRepository = new SessionRepository();
    IUserRepository userRepository = new UserRepository();
    IUserService userService = new UserService(userRepository);
    IPropertyService propertyService = new PropertyService();
    ISessionService sessionService = new SessionService(sessionRepository, userService, propertyService);

    @Test(expected = EmptySignatureSessionException.class)
    public void validateWithEmptySignatureTest() {
        User user = userService.create("login", "password", Role.USER);
        Session session = new Session(1L, user.getId(), null);
        sessionService.validate(session, user.getRole());
    }

    @Test(expected = EmptyTimestampSessionException.class)
    public void validateWithEmptyTimestampTest() {
        User user = userService.create("login", "password", Role.USER);
        Session session = new Session(null, user.getId(), "signature");
        sessionService.validate(session, user.getRole());
    }

    @Test(expected = EmptyUserIdSessionException.class)
    public void validateWithEmptyUserIdTest() {
        User user = userService.create("login", "password", Role.USER);
        Session session = new Session(1L, null, "signature");
        sessionService.validate(session, user.getRole());
    }

    @Test
    public void validate() {
        User user = userService.create("login", "password", Role.USER);
        Session session = new Session(1L, user.getId(), "signature");
        sessionService.validate(session, user.getRole());
    }

    @Test(expected = AccessDeniedException.class)
    public void checkAccessDataError() {
        User user = userService.create("login", "password", Role.USER);
        User userTest = userService.create("login", "password.", Role.USER);
        if (!sessionService.checkDataAccess(user.getLogin(), userTest.getPassword())) {
            throw new AccessDeniedException();
        }
        ;
    }

    @Test
    public void checkAccessDataNoError() {
        User user = userService.create("login", "password", Role.USER);
        User userTest = userService.create("login", "password.", Role.USER);
        if (!sessionService.checkDataAccess(user.getLogin(), user.getPassword())) {
            throw new AccessDeniedException();
        }
        ;
    }

    @Test
    public void openSession() {
        User user = userService.create("login", "password", Role.USER);
        Session session = new Session();
        Assert.assertNull(session.getUserId());
        Assert.assertNull(session.getTimestamp());
        Assert.assertNull(session.getSignature());
        session = sessionService.open(user.getLogin(), user.getPassword());
        Assert.assertNotNull(session.getUserId());
        Assert.assertNotNull(session.getTimestamp());
        Assert.assertNotNull(session.getSignature());
        Assert.assertEquals(session.getUserId(), user.getId());
    }

    @Test
    public void closeSession() {
        User user = userService.create("login", "password", Role.USER);
        Session session;
        session = sessionService.open(user.getLogin(), user.getPassword());
        Assert.assertFalse(sessionService.getListSession(session).isEmpty());
        sessionService.close(session);
        Assert.assertTrue(sessionService.getListSession(session).isEmpty());
    }

}
