package service;

import org.junit.Assert;
import org.junit.Test;
import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.exception.empty.EmptyNameException;
import ru.anenkov.tm.exception.empty.EmptyUserIdException;
import ru.anenkov.tm.exception.system.IncorrectIndexException;
import ru.anenkov.tm.repository.TaskRepository;
import ru.anenkov.tm.service.TaskService;

public class TaskServiceTest {

    @Test
    public void createWithNameTest() {
        TaskRepository taskRepository = new TaskRepository();
        TaskService taskService = new TaskService(taskRepository);
        User user = new User();
        Assert.assertTrue(taskService.findAll(user.getId()).isEmpty());
        taskService.create(user.getId(), "first");
        Assert.assertFalse(taskService.findAll(user.getId()).isEmpty());
        Assert.assertEquals(taskService.findAll(user.getId()).size(), 1);
    }

    @Test
    public void createWithDescriptionTest() {
        TaskRepository taskRepository = new TaskRepository();
        TaskService taskService = new TaskService(taskRepository);
        User user = new User();
        Assert.assertTrue(taskService.findAll(user.getId()).isEmpty());
        taskService.create(user.getId(), "first", "first description");
        Assert.assertFalse(taskService.findAll(user.getId()).isEmpty());
        Assert.assertEquals(taskService.findAll(user.getId()).size(), 1);
    }

    @Test(expected = EmptyNameException.class)
    public void createNullNameTest() {
        TaskRepository taskRepository = new TaskRepository();
        TaskService taskService = new TaskService(taskRepository);
        User user = new User();
        taskService.create(user.getId(), null);
    }

    @Test
    public void addTest() {
        TaskRepository taskRepository = new TaskRepository();
        TaskService taskService = new TaskService(taskRepository);
        User user = new User();
        Task task = new Task("name", "description", user.getId());
        Assert.assertEquals(taskService.findAll(user.getId()).size(), 0);
        taskService.add(user.getId(), task);
        Assert.assertEquals(taskService.findAll(user.getId()).size(), 1);
    }

    @Test
    public void findOneByIndex() {
        TaskRepository taskRepository = new TaskRepository();
        TaskService taskService = new TaskService(taskRepository);
        User user = new User();
        Task task = new Task("name", "description", user.getId());
        Task testTask;
        taskService.add(user.getId(), task);
        Assert.assertEquals(taskService.findAll(user.getId()).size(), 1);
        testTask = taskService.findOneByIndex(user.getId(), 0);
        Assert.assertNotNull(testTask);
    }

    @Test
    public void findOneByName() {
        TaskRepository taskRepository = new TaskRepository();
        TaskService taskService = new TaskService(taskRepository);
        User user = new User();
        Task task = new Task("name", "description", user.getId());
        Task testTask;
        taskService.add(user.getId(), task);
        Assert.assertEquals(taskService.findAll(user.getId()).size(), 1);
        testTask = taskService.findOneByName(user.getId(), "name");
        Assert.assertNotNull(testTask);
    }

    @Test
    public void findOneById() {
        TaskRepository taskRepository = new TaskRepository();
        TaskService taskService = new TaskService(taskRepository);
        User user = new User();
        Task task = new Task("name", "description", user.getId());
        Task testTask;
        taskService.add(user.getId(), task);
        Assert.assertEquals(taskService.findAll(user.getId()).size(), 1);
        testTask = taskService.findOneById(user.getId(), task.getId());
        Assert.assertNotNull(testTask);
    }

    @Test(expected = EmptyUserIdException.class)
    public void emptyUserIdTest() {
        TaskRepository taskRepository = new TaskRepository();
        TaskService taskService = new TaskService(taskRepository);
        User user = new User();
        Task task = new Task("name", "description", user.getId());
        Task testTask;
        testTask = taskService.findOneById(null, task.getId());
    }

    @Test(expected = EmptyNameException.class)
    public void emptyNameTest() {
        TaskRepository taskRepository = new TaskRepository();
        TaskService taskService = new TaskService(taskRepository);
        User user = new User();
        Task task = new Task("name", "description", user.getId());
        Task testTask;
        testTask = taskService.findOneByName(user.getId(), null);
    }

    @Test(expected = IncorrectIndexException.class)
    public void incorrectIndexTest() {
        TaskRepository taskRepository = new TaskRepository();
        TaskService taskService = new TaskService(taskRepository);
        Integer index = null;
        User user = new User();
        Task task = new Task("name", "description", user.getId());
        Task testTask;
        taskService.add(user.getId(), task);
        Assert.assertEquals(taskService.findAll(user.getId()).size(), 1);
        testTask = taskService.findOneByIndex(user.getId(), index);
        Assert.assertNotNull(testTask);
    }

}
