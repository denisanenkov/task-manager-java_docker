package service;

import org.junit.Assert;
import org.junit.Test;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.exception.empty.EmptyEmailException;
import ru.anenkov.tm.exception.empty.EmptyLoginException;
import ru.anenkov.tm.exception.empty.EmptyPasswordException;
import ru.anenkov.tm.repository.UserRepository;
import ru.anenkov.tm.service.UserService;
import ru.anenkov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public class UserServiceTest {

    @Test(expected = EmptyLoginException.class)
    public void createNullLoginTest() {
        final UserRepository userRepository = new UserRepository();
        final UserService userService = new UserService(userRepository);
        final User user = userService.create(null, "password");
    }

    @Test(expected = EmptyPasswordException.class)
    public void createNullPassTest() {
        final UserRepository userRepository = new UserRepository();
        final UserService userService = new UserService(userRepository);
        final User user = userService.create("login", null);
    }

    @Test(expected = EmptyEmailException.class)
    public void createNullEmailTest() {
        final UserRepository userRepository = new UserRepository();
        final UserService userService = new UserService(userRepository);
        final User user = userService.create("login", "password", (String) null);
    }

    public void createTest() {
        final UserRepository userRepository = new UserRepository();
        final UserService userService = new UserService(userRepository);
        User user = new User();
        Assert.assertNull(user.getLogin());
        Assert.assertNull(user.getPasswordHash());
        Assert.assertNull(user.getEmail());
        user = userService.create("login", "password", "test@mail.ru");
        Assert.assertEquals("login", user.getLogin());
        Assert.assertEquals("password", user.getPasswordHash());
        Assert.assertEquals("test@mail.ru", user.getEmail());
    }

    @Test
    public void updateFirstNameTest() {
        final UserRepository userRepository = new UserRepository();
        final UserService userService = new UserService(userRepository);
        User user = userService.create("login", "password", "test@mail.ru");
        assert user != null;
        user.setFirstName("Ivan");
        Assert.assertEquals("Ivan", user.getFirstName());
        userService.updateUserFirstName(user.getId(), "Denis");
        Assert.assertEquals("Denis", user.getFirstName());
    }

    @Test
    public void updateMiddleNameTest() {
        final UserRepository userRepository = new UserRepository();
        final UserService userService = new UserService(userRepository);
        User user = userService.create("login", "password", "test@mail.ru");
        assert user != null;
        user.setMiddleName("First");
        Assert.assertEquals("First", user.getMiddleName());
        userService.updateUserMiddleName(user.getId(), "Second");
        Assert.assertEquals("Second", user.getMiddleName());
    }

    @Test
    public void updateLastNameTest() {
        final UserRepository userRepository = new UserRepository();
        final UserService userService = new UserService(userRepository);
        User user = userService.create("login", "password", "test@mail.ru");
        assert user != null;
        user.setLastName("Ivanov");
        Assert.assertEquals("Ivanov", user.getLastName());
        userService.updateUserLastName(user.getId(), "Petrov");
        Assert.assertEquals("Petrov", user.getLastName());
    }

    @Test
    public void updateEmailTest() {
        final UserRepository userRepository = new UserRepository();
        final UserService userService = new UserService(userRepository);
        User user = userService.create("login", "password", "test@mail.ru");
        assert user != null;
        Assert.assertEquals("test@mail.ru", user.getEmail());
        userService.updateUserEmail(user.getId(), "newTest@mail.ru");
        Assert.assertEquals("newTest@mail.ru", user.getEmail());
    }

    @Test
    public void updatePassTest() {
        final UserRepository userRepository = new UserRepository();
        final UserService userService = new UserService(userRepository);
        User user = userService.create("login", "password", "test@mail.ru");
        assert user != null;
        Assert.assertEquals(HashUtil.salt("password"), user.getPasswordHash());
        userService.updatePassword(user.getId(), "newPassword");
        Assert.assertEquals("newPassword", user.getPassword());
    }

    @Test
    public void findUsersTest() {
        final UserRepository userRepository = new UserRepository();
        final UserService userService = new UserService(userRepository);
        User user1 = userService.create("login1", "password1", "test1@mail.ru");
        User user2 = userService.create("login2", "password2", "test2@mail.ru");
        User user3 = userService.create("login3", "password3", "test3@mail.ru");
        List<User> users = new ArrayList<>();
        Assert.assertEquals(users.size(), 0);
        Assert.assertTrue(users.isEmpty());
        users = userService.findAll();
        Assert.assertEquals(users.size(), 3);
        Assert.assertEquals(userService.findAll().size(), 3);
    }

    @Test
    public void removeUsersTest() {
        final UserRepository userRepository = new UserRepository();
        final UserService userService = new UserService(userRepository);
        User user1 = userService.create("login1", "password1", "test1@mail.ru");
        User user2 = userService.create("login2", "password2", "test2@mail.ru");
        User user3 = userService.create("login3", "password3", "test3@mail.ru");
        User user4 = userService.create("login4", "password4", "test4@mail.ru");
        Assert.assertEquals(userService.findAll().size(), 4);
        User testUser = null;
        testUser = userService.removeUser(user1);
        Assert.assertNotNull(testUser);
        Assert.assertEquals(userService.findAll().size(), 3);
        testUser = null;
        testUser = userService.removeById(user2.getId());
        Assert.assertNotNull(testUser);
        Assert.assertEquals(userService.findAll().size(), 2);
        testUser = null;
        testUser = userService.removeByEmail("test3@mail.ru");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(userService.findAll().size(), 1);
        testUser = null;
        testUser = userService.removeByLogin("login4");
        Assert.assertNotNull(testUser);
        Assert.assertEquals(userService.findAll().size(), 0);
        testUser = null;
    }

}
