package service;

import org.junit.Assert;
import org.junit.Test;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.exception.empty.EmptyNameException;
import ru.anenkov.tm.exception.empty.EmptyUserIdException;
import ru.anenkov.tm.exception.system.IncorrectIndexException;
import ru.anenkov.tm.repository.ProjectRepository;
import ru.anenkov.tm.service.ProjectService;

public class ProjectServiceTest {

    @Test
    public void createWithNameTest() {
        ProjectRepository projectRepository = new ProjectRepository();
        ProjectService projectService = new ProjectService(projectRepository);
        User user = new User();
        Assert.assertTrue(projectService.findAll(user.getId()).isEmpty());
        projectService.create(user.getId(), "first");
        Assert.assertFalse(projectService.findAll(user.getId()).isEmpty());
        Assert.assertEquals(projectService.findAll(user.getId()).size(), 1);
    }

    @Test
    public void createWithDescriptionTest() {
        ProjectRepository projectRepository = new ProjectRepository();
        ProjectService projectService = new ProjectService(projectRepository);
        User user = new User();
        Assert.assertTrue(projectService.findAll(user.getId()).isEmpty());
        projectService.create(user.getId(), "first", "first description");
        Assert.assertFalse(projectService.findAll(user.getId()).isEmpty());
        Assert.assertEquals(projectService.findAll(user.getId()).size(), 1);
    }

    @Test(expected = EmptyNameException.class)
    public void createNullNameTest() {
        ProjectRepository projectRepository = new ProjectRepository();
        ProjectService projectService = new ProjectService(projectRepository);
        User user = new User();
        projectService.create(user.getId(), null);
    }

    @Test
    public void addTest() {
        ProjectRepository projectRepository = new ProjectRepository();
        ProjectService projectService = new ProjectService(projectRepository);
        User user = new User();
        Project project = new Project("name", "description", user.getId());
        Assert.assertEquals(projectService.findAll(user.getId()).size(), 0);
        projectService.add(user.getId(), project);
        Assert.assertEquals(projectService.findAll(user.getId()).size(), 1);
    }

    @Test
    public void findOneByIndex() {
        ProjectRepository projectRepository = new ProjectRepository();
        ProjectService projectService = new ProjectService(projectRepository);
        User user = new User();
        Project project = new Project("name", "description", user.getId());
        Project testProject;
        projectService.add(user.getId(), project);
        Assert.assertEquals(projectService.findAll(user.getId()).size(), 1);
        testProject = projectService.findOneByIndex(user.getId(), 0);
        Assert.assertNotNull(testProject);
    }

    @Test
    public void findOneByName() {
        ProjectRepository projectRepository = new ProjectRepository();
        ProjectService projectService = new ProjectService(projectRepository);
        User user = new User();
        Project project = new Project("name", "description", user.getId());
        Project testProject;
        projectService.add(user.getId(), project);
        Assert.assertEquals(projectService.findAll(user.getId()).size(), 1);
        testProject = projectService.findOneByName(user.getId(), "name");
        Assert.assertNotNull(testProject);
    }

    @Test
    public void findOneById() {
        ProjectRepository projectRepository = new ProjectRepository();
        ProjectService projectService = new ProjectService(projectRepository);
        User user = new User();
        Project project = new Project("name", "description", user.getId());
        Project testProject;
        projectService.add(user.getId(), project);
        Assert.assertEquals(projectService.findAll(user.getId()).size(), 1);
        testProject = projectService.findOneById(user.getId(), project.getId());
        Assert.assertNotNull(testProject);
    }

    @Test(expected = EmptyUserIdException.class)
    public void emptyUserIdTest() {
        ProjectRepository projectRepository = new ProjectRepository();
        ProjectService projectService = new ProjectService(projectRepository);
        User user = new User();
        Project project = new Project("name", "description", user.getId());
        Project testProject;
        testProject = projectService.findOneById(null, project.getId());
    }

    @Test(expected = EmptyNameException.class)
    public void emptyNameTest() {
        ProjectRepository projectRepository = new ProjectRepository();
        ProjectService projectService = new ProjectService(projectRepository);
        User user = new User();
        Project project = new Project("name", "description", user.getId());
        Project testProject;
        testProject = projectService.findOneByName(user.getId(), null);
    }

    @Test(expected = IncorrectIndexException.class)
    public void incorrectIndexTest() {
        ProjectRepository projectRepository = new ProjectRepository();
        ProjectService projectService = new ProjectService(projectRepository);
        Integer index = null;
        User user = new User();
        Project project = new Project("name", "description", user.getId());
        Project testProject;
        projectService.add(user.getId(), project);
        Assert.assertEquals(projectService.findAll(user.getId()).size(), 1);
        testProject = projectService.findOneByIndex(user.getId(), index);
        Assert.assertNotNull(testProject);
    }

}
