package suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import util.FormatBytesUtilTest;
import util.HashUtilTest;
import util.SignatureUtilTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        FormatBytesUtilTest.class,
        HashUtilTest.class,
        SignatureUtilTest.class})
public class UtilSuite {
}
