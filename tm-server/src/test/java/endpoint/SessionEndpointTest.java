package endpoint;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.anenkov.tm.bootstrap.Bootstrap;
import ru.anenkov.tm.dto.Result;
import ru.anenkov.tm.dto.Success;
import ru.anenkov.tm.endpoint.SessionEndpoint;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.repository.UserRepository;
import ru.anenkov.tm.service.UserService;

import java.util.List;

public class SessionEndpointTest {

    Bootstrap bootstrap = new Bootstrap();
    SessionEndpoint sessionEndpoint = new SessionEndpoint(bootstrap);

    @Before
    public void before() {
        bootstrap.initUsers();
    }

    @Test
    public void openSessionTest() {
        Assert.assertTrue(bootstrap.getSessionService().getList().isEmpty());
        Session session = sessionEndpoint.openSession("1", "1");
        List<Session> sessionList = sessionEndpoint.allSessions(session);
        Assert.assertFalse(bootstrap.getSessionService().getList().isEmpty());
        Assert.assertFalse(sessionList.isEmpty());
    }

    @Test
    public void closeSessionTest() {
        Assert.assertTrue(bootstrap.getSessionService().getList().isEmpty());
        Session session = sessionEndpoint.openSession("1", "1");
        List<Session> sessionList = sessionEndpoint.allSessions(session);
        Assert.assertFalse(sessionList.isEmpty());
        sessionEndpoint.closeSession(session);
        sessionList = sessionEndpoint.allSessions(session);
        Assert.assertTrue(sessionList.isEmpty());
    }

    @Test
    public void findSessionAllTest() {
        Assert.assertTrue(bootstrap.getSessionService().getList().isEmpty());
        Session session1 = sessionEndpoint.openSession("1", "1");
        Session session01 = sessionEndpoint.openSession("1", "1");
        Session session2 = sessionEndpoint.openSession("2", "2");
        Session session3 = sessionEndpoint.openSession("test", "test");
        Assert.assertFalse(sessionEndpoint.allSessions(session1).isEmpty());
        Assert.assertEquals(sessionEndpoint.allSessions(session1).size(), 2);
        Assert.assertEquals(sessionEndpoint.allSessions(session2).size(), 1);
        Assert.assertEquals(sessionEndpoint.allSessions(session3).size(), 1);
        Assert.assertFalse(bootstrap.getSessionService().getList().isEmpty());
    }

    @Test
    public void closeAllSessionsTest() {
        Assert.assertTrue(bootstrap.getSessionService().getList().isEmpty());
        Session session1 = sessionEndpoint.openSession("1", "1");
        Session session2 = sessionEndpoint.openSession("1", "1");
        Session session3 = sessionEndpoint.openSession("1", "1");
        Assert.assertFalse(sessionEndpoint.allSessions(session1).isEmpty());
        Assert.assertEquals(sessionEndpoint.allSessions(session1).size(), 3);
        Result result = sessionEndpoint.closeSessionAll(session1);
        Assert.assertEquals(sessionEndpoint.allSessions(session1).size(), 0);
        Assert.assertTrue(sessionEndpoint.allSessions(session1).isEmpty());

    }

}
