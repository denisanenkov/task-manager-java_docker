package endpoint;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.anenkov.tm.bootstrap.Bootstrap;
import ru.anenkov.tm.endpoint.SessionEndpoint;
import ru.anenkov.tm.endpoint.TaskEndpoint;
import ru.anenkov.tm.endpoint.UserEndpoint;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskEndpointTest {

    final Bootstrap bootstrap = new Bootstrap();
    final TaskEndpoint taskEndpoint = new TaskEndpoint(bootstrap);
    final SessionEndpoint sessionEndpoint = new SessionEndpoint(bootstrap);

    @Before
    public void before() {
        bootstrap.initUsers();
    }

    @Test
    public void createTest() {
        final Session session = sessionEndpoint.openSession("1", "1");
        Assert.assertEquals(taskEndpoint.findAll(session).size(), 0);
        taskEndpoint.create(session, "taskName", "taskDescription");
        Assert.assertEquals(taskEndpoint.findAll(session).size(), 1);
    }

    @Test
    public void clearTasks() {
        final Session session = sessionEndpoint.openSession("1", "1");
        taskEndpoint.create(session, "taskName1", "taskDescription1");
        taskEndpoint.create(session, "taskName2", "taskDescription2");
        Assert.assertEquals(taskEndpoint.findAll(session).size(), 2);
        taskEndpoint.clear(session);
        Assert.assertEquals(taskEndpoint.findAll(session).size(), 0);
    }

    @Test
    public void findByParameters() {
        final Session session = sessionEndpoint.openSession("1", "1");
        taskEndpoint.create(session, "taskName1", "taskDescription1");
        taskEndpoint.create(session, "taskName2", "taskDescription2");
        taskEndpoint.create(session, "taskName3", "taskDescription3");
        Task task1 = null, task2 = null, task3 = null;
        Assert.assertNull(task1);
        task1 = taskEndpoint.findOneByName(session, "taskName1");
        Assert.assertNotNull(task1);
        Assert.assertNull(task2);
        task2 = taskEndpoint.findOneByIndex(session, 1);
        Assert.assertNotNull(task2);
        Assert.assertNull(task3);
        Task task = taskEndpoint.findOneByName(session, "taskName3");
        task3 = taskEndpoint.findOneById(session, task.getId());
        Assert.assertNotNull(task3);
        Assert.assertEquals(task1.getName(), "taskName1");
        Assert.assertEquals(task2.getDescription(), "taskDescription2");
        Assert.assertEquals(task3.getName(), "taskName3");
    }

    @Test
    public void updateByParameters() {
        final Session session = sessionEndpoint.openSession("1", "1");
        taskEndpoint.create(session, "taskName1", "taskDescription1");
        taskEndpoint.create(session, "taskName2", "taskDescription2");
        Task task1 = taskEndpoint.findOneByName(session, "taskName1");
        Task task2 = taskEndpoint.findOneByName(session, "taskName2");
        Assert.assertEquals(task1.getName(), "taskName1");
        Assert.assertEquals(task2.getName(), "taskName2");
        taskEndpoint.updateTaskByIndex(session, 0, "newName1", "new Description1");
        taskEndpoint.updateTaskById(session, task2.getId(), "newName2", "new Description2");
        Assert.assertEquals(task1.getName(), "newName1");
        Assert.assertEquals(task2.getName(), "newName2");
        Assert.assertEquals(task1.getDescription(), "new Description1");
        Assert.assertEquals(task2.getDescription(), "new Description2");
    }

    @Test
    public void removeByParameter() {
        final Session session = sessionEndpoint.openSession("1", "1");
        Assert.assertTrue(taskEndpoint.findAll(session).isEmpty());
        taskEndpoint.create(session, "taskName1", "taskDescription1");
        taskEndpoint.create(session, "taskName2", "taskDescription2");
        taskEndpoint.create(session, "taskName3", "taskDescription3");
        Assert.assertEquals(taskEndpoint.findAll(session).size(), 3);
        Task testTask = taskEndpoint.findOneByName(session, "taskName2");
        taskEndpoint.removeOneByName(session, "taskName1");
        Assert.assertEquals(taskEndpoint.findAll(session).size(), 2);
        taskEndpoint.removeOneById(session, testTask.getId());
        Assert.assertEquals(taskEndpoint.findAll(session).size(), 1);
        taskEndpoint.removeOneByIndex(session, 0);
        Assert.assertEquals(taskEndpoint.findAll(session).size(), 0);
    }

    @Test
    public void getListTest() {
        final Session session = sessionEndpoint.openSession("1", "1");
        taskEndpoint.create(session, "taskName1", "taskDescription1");
        taskEndpoint.create(session, "taskName2", "taskDescription2");
        taskEndpoint.create(session, "taskName3", "taskDescription3");
        List<Task> taskList = new ArrayList<>();
        Assert.assertTrue(taskList.isEmpty());
        Assert.assertEquals(taskList.size(), 0);
        taskList = taskEndpoint.getList(session);
        Assert.assertFalse(taskList.isEmpty());
        Assert.assertEquals(taskList.size(), 3);
    }

}
