package ru.anenkov.tm.repository;

import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.repository.ISessionRepository;
import ru.anenkov.tm.entity.Session;

import java.util.ArrayList;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    @Nullable
    public void removeByUserId(String userId) {
        if (userId == null || userId.isEmpty()) return;
        final List<Session> sessions = findByUserId(userId);
        for (final Session session : sessions) {
            removeEntity(session);
        }
    }

    @Override
    @Nullable
    public List<Session> findByUserId(String userId) {
        if (userId == null || userId.isEmpty()) return null;
        final List<Session> sessions = findAll();
        final List<Session> result = new ArrayList<>();
        for (final Session session : sessions) {
            if (userId.equals(session.getUserId())) {
                result.add(session);
            }
        }
        return result;
    }

    @Override
    public @Nullable List<Session> getList() {
        return findAll();
    }

    @Override
    public void clearAllSessions() {
        List<Session> sessionList = new ArrayList<>();
        for (Session session : sessionList) {
            remove(session);
        }
    }

}
