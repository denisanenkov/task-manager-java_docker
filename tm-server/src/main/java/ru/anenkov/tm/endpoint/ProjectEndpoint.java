package ru.anenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.endpoint.IProjectEndpoint;
import ru.anenkov.tm.api.service.IServiceLocator;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    public ProjectEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    public ProjectEndpoint() {
        super(null);
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void createProject(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "name") final String name,
            @NotNull @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().create(session.getUserId(), name, description);
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void addProject(
            @NotNull @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "tasks") final Project project
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().add(session.getUserId(), project);
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void removeProject(
            @NotNull @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "tasks") final Project project
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().remove(session.getUserId(), project);
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public List<Project> findAllProjects(
            @NotNull @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void clear(
            @NotNull @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clear(session.getUserId());
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public Project findOneByIndexProject(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public Project findOneByNameProject(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneByName(session.getUserId(), name);
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public Project findOneByIdProject(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneById(session.getUserId(), id);
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public @NotNull Project removeOneByIndexProject(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public @NotNull Project removeOneByNameProject(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneByName(session.getUserId(), name);
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public @NotNull Project removeOneByIdProject(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneById(session.getUserId(), id);
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public @NotNull Project updateByIdProject(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "id") final String id,
            @NotNull @WebParam(name = "name") final String name,
            @NotNull @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        Project project = serviceLocator.getProjectService().updateProjectById(session.getUserId(), id, name, description);
        return project;
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public @NotNull Project updateByIndexProject(
            @NotNull @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "index") final Integer index,
            @NotNull @WebParam(name = "name") final String name,
            @NotNull @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        Project project = serviceLocator.getProjectService().updateProjectByIndex(session.getUserId(),
                index, name, description);
        return project;
    }

    @WebMethod
    @SneakyThrows
    @Override
    public void loadProject(
            @NotNull @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "tasks") final List project
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().load(project);
    }

    @WebMethod
    @SneakyThrows
    @Override
    @Nullable
    public List<Project> getListProjects(
            @NotNull @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().getList();
    }

}
