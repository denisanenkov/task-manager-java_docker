package ru.anenkov.tm.api.service;

import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.enumeration.Role;

import java.util.List;

public interface ISessionService extends IService<Session> {

    boolean checkDataAccess(String login, String password);

    Session open(String login, String password);

    Session sign(Session session);

    User getUser(Session session);

    String getUserId(Session session);

    List<Session> getListSession(Session session);

    void close(Session session);

    void closeAll(Session session);

    void validate(Session session, Role role);

    void validate(Session session);

    void signOutByLogin(String login);

    void signOutByUserId(String userId);

    boolean isValid(Session session);

    void clearAllSessions();

}
