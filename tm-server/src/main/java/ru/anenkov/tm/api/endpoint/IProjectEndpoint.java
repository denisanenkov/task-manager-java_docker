package ru.anenkov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.entity.Project;
import ru.anenkov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    @SneakyThrows
    void createProject(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "name", partName = "name") final String name,
            @NotNull @WebParam(name = "description", partName = "description") final String description
    );

    @WebMethod
    @SneakyThrows
    void addProject(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "session", partName = "session") final Project project
    );

    @WebMethod
    @SneakyThrows
    void removeProject(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "task", partName = "task") final Project project
    );

    @WebMethod
    @SneakyThrows
    List<Project> findAllProjects(
            @NotNull @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    @SneakyThrows
    void clear(
            @NotNull @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    @SneakyThrows
    Project findOneByIndexProject(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "index", partName = "index") final Integer index
    );

    @WebMethod
    @SneakyThrows
    Project findOneByNameProject(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "name", partName = "name") final String name
    );

    @WebMethod
    @SneakyThrows
    Project findOneByIdProject(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "id", partName = "id") final String id
    );

    @WebMethod
    @SneakyThrows
    @NotNull Project removeOneByIndexProject(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "index", partName = "index") final Integer index
    );

    @WebMethod
    @SneakyThrows
    @NotNull Project removeOneByNameProject(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "name", partName = "name") final String name
    );

    @WebMethod
    @SneakyThrows
    @NotNull Project removeOneByIdProject(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "id", partName = "id") final String id
    );

    @WebMethod
    @SneakyThrows
    @NotNull Project updateByIdProject(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "id", partName = "id") final String id,
            @NotNull @WebParam(name = "name", partName = "name") final String name,
            @NotNull @WebParam(name = "description", partName = "description") final String description
    );

    @WebMethod
    @SneakyThrows
    @NotNull Project updateByIndexProject(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "index", partName = "index") final Integer index,
            @NotNull @WebParam(name = "name", partName = "name") final String name,
            @NotNull @WebParam(name = "description", partName = "description") final String description
    );

    @WebMethod
    void loadProject(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "task", partName = "task") final List project
    );

    @WebMethod
    List<Project> getListProjects(
            @NotNull @WebParam(name = "session", partName = "session") final Session session
    );

}
