package ru.anenkov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.io.IOException;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @WebMethod
    @Nullable List<User> findAllUser(
            @NotNull @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    @Nullable User createUser(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "login", partName = "login") final String login,
            @NotNull @WebParam(name = "password", partName = "password") final String password
    );

    @WebMethod
    @Nullable User findByIdUser(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "id", partName = "id") final String id
    );

    @WebMethod
    @Nullable User findByLoginUser(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "login", partName = "login") final String login
    );

    @WebMethod
    @Nullable User findByEmailUser(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @NotNull @WebParam(name = "email", partName = "email") final String email
    );

    @WebMethod
    @Nullable User updateUserFirstName(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "newFirstName", partName = "newFirstName") final String newFirstName
    );

    @WebMethod
    @Nullable User updateUserMiddleName(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "newMiddleName", partName = "newMiddleName") final String newMiddleName
    );

    @WebMethod
    @Nullable User updateUserLastName(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "newLastName", partName = "newLastName") final String newLastName
    );

    @WebMethod
    @Nullable User updateUserEmail(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "newEmail", partName = "newEmail") final String newEmail
    );

    @WebMethod
    @Nullable User updatePasswordUser(
            @NotNull @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "newPassword", partName = "newPassword") final String newPassword
    );

    @WebMethod
    @Nullable List<User> getListUser(
            @NotNull @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    User showUserProfile(
            @NotNull @WebParam(name = "session") final Session session
    );

}
